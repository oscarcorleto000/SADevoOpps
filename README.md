# SoftwareAvanzado
Repositorio destinado a las tareas del curso de Software Avanzado, segundo semestre 2020
# Links para artefactos
Lista de pipelines para buscar el https://gitlab.com/oscarcorleto000/SADevoOpps/-/pipelines
Lista de la carpeta dist https://gitlab.com/oscarcorleto000/SADevoOpps/-/jobs/758757002/artifacts/browse

# Tarea 7 - 201602811
Cada carpeta contiene el código de cada "API" destinada para cada servicio. Para poder poner en funcionamiento cada servicio, se coloca la carpeta en el servidor donde se desea correr y se inicia el archivo index.js de node.

```
node index.js
```
